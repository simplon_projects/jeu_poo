class Character {
    constructor(name, health, strong) {
        this.name = name;
        this.health = health;
        this.strong = strong;
        this.xp = 0;
    }
    describe() {
        console.log(`${this.name} has ${this.health} life points, ${this.strong} as strong points and ${this.xp} XP.`);
    }
    attack(target) {
        if (this.health > 0) {
            const damages = this.strong;
            console.log(`${this.name} attack ${target.name} and damage of ${damages} points`);
            target.health -= damages;
            if (target.health > 0) {
                console.log(`${target.name} has still ${target.health} life points`);
            } else {
                target.health = 0;
                const bonusXP = 10;
                console.log(`${this.name} killed ${target.name} and win ${bonusXP} xp points`);
                this.xp += bonusXP;
            }
        } else {
            console.log(
                `${this.name} has no more life point and is not able to attack`
            );
        }
    }
}